# TestList-Iterator-method-in-Java


import java.util.*;

public class TestListIterator {
 
 public static void main(String[] args){
  
  LinkedList myList = new LinkedList();
  myList.add("hello");
  myList.add("ciao");
  myList.add("them");
  myList.add("me");
  myList.add("you");
  myList.add("she");
  myList.addFirst("she");
  myList.add(myList.size(), "last");
  myList.add(0, "first");
  myList.add(myList.size() / 2, "middle");
  System.out.println(myList);
  
  ListIterator iter = myList.listIterator();
  while (iter.hasNext())
  if (iter.next().equals("me"))
  iter.set("we");
   
   while (iter.hasPrevious()) {
   if (iter.previous().equals("ciao")) {
       iter.remove();
       iter.add("good-bye");
    }
   }
   
   System.out.println(myList);
  }
 }  
       
